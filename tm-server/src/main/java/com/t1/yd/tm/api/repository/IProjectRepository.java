package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.dto.model.ProjectDTO;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<ProjectDTO> {


    @Update("UPDATE projects " +
            "SET user_id = #{userId}, " +
            "name = #{name}, " +
            "description = #{description}, " +
            "status = #{status}" +
            " WHERE id = #{id}")
    void update(@NotNull ProjectDTO projectDTO);

    @Insert("INSERT INTO projects " +
            "(id, created, user_id, name, description, status) " +
            "VALUES (#{id}, #{created}, #{userId}, #{name}, #{description}, #{status})")
    void add(@NotNull ProjectDTO projectDTO);


    @Select("SELECT * FROM projects order by #{sort}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @NotNull List<ProjectDTO> findAll(@NotNull String sort);


    @Select("SELECT * FROM projects WHERE user_id = #{userId} order by #{sortField}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @NotNull List<ProjectDTO> findAllWithUserId(@Param("userId") @NotNull String userId, @Param("sort") @NotNull String sort);


    @Delete("DELETE FROM projects")
    void clear();


    @Delete("DELETE FROM projects WHERE user_id = #{userId}")
    void clearWithUserId(@NotNull String userId);


    @Select("SELECT * FROM projects WHERE id = #{id} LIMIT 1")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneById(@NotNull String id);


    @Select("SELECT * from (SELECT row_number() " +
            "OVER (order by id) as rnum, * FROM projects) t1 WHERE rnum = #{index}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneByIndex(@NotNull Integer index);

    @Delete("DELETE FROM projects WHERE id = #{id}")
    void removeById(@NotNull String id);

    @Select("SELECT * FROM projects WHERE user_id = #{userId} AND id = #{id}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneByIdWithUserId(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);


    @Select("SELECT * FROM (SELECT row_number() " +
            "OVER (order by id) as rnum, * FROM projects WHERE user_id = #{userId}) t1 WHERE rnum = #{index}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneByIndexWithUserId(@Param("userId") @NotNull String userId,
                                                  @Param("index") @NotNull Integer index);


}