package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.dto.model.TaskDTO;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<TaskDTO> {

    @Select("SELECT * FROM tasks WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<TaskDTO> findAllByProjectId(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId);


    @Update("UPDATE tasks " +
            "SET user_id = #{userId}, " +
            "   name = #{name}, " +
            "   description = #{description}, " +
            "   status = #{status}," +
            "   project_id = #{projectId}" +
            " WHERE id = #{id}")
    void update(@NotNull TaskDTO taskDTO);


    @Insert("INSERT INTO tasks " +
            "(id, created, user_id, name, description, status, project_id) " +
            "VALUES (#{id}, #{created}, #{userId}, #{name}, #{description}, #{status}, #{projectId})")
    void add(@NotNull TaskDTO taskDTO);


    @Select("SELECT * FROM tasks order by #{sort}")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<TaskDTO> findAll(@NotNull String sort);


    @Select("SELECT * FROM tasks WHERE user_id = #{userId} order by #{sortField}")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<TaskDTO> findAllWithUserId(@Param("userId") @NotNull String userId, @Param("sortField") @NotNull String sortField);


    @Delete("DELETE FROM tasks")
    void clear();


    @Delete("DELETE FROM tasks WHERE user_id = #{userId}")
    void clearWithUserId(@NotNull String userId);


    @Select("SELECT * FROM tasks WHERE id = #{id} LIMIT 1")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable TaskDTO findOneById(@NotNull String id);


    @Select("SELECT * from (SELECT row_number() " +
            "OVER (order by id) as rnum, * FROM tasks) t1 WHERE rnum = #{index}")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable TaskDTO findOneByIndex(@NotNull Integer index);


    @Delete("DELETE FROM tasks WHERE id = #{id}")
    void removeById(@NotNull String id);


    @Select("SELECT * FROM tasks WHERE user_id = #{userId} AND id = #{id}")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable TaskDTO findOneByIdWithUserId(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);


    @Select("SELECT * FROM (SELECT row_number() " +
            "OVER (order by id) as rnum, * FROM tasks WHERE user_id = #{userId}) t1 WHERE rnum = #{index}")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable TaskDTO findOneByIndexWithUserId(@Param("userId") @NotNull String userId,
                                               @Param("index") @NotNull Integer index);


}
