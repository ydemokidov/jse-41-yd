package com.t1.yd.tm.api.service;

import com.t1.yd.tm.dto.model.SessionDTO;

public interface ISessionService extends IUserOwnedService<SessionDTO> {
}
