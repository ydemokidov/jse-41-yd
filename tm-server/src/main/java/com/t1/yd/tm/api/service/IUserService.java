package com.t1.yd.tm.api.service;

import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserService extends IService<UserDTO> {

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password);

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @NotNull String email);

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @NotNull String email, @NotNull Role role);

    @NotNull
    UserDTO removeByLogin(@NotNull String login);

    @NotNull
    UserDTO removeByEmail(@NotNull String email);

    @NotNull
    UserDTO setPassword(@NotNull String id, @NotNull String password);

    @NotNull
    UserDTO updateUser(@NotNull String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    void lockByLogin(@NotNull String login);

    void unlockByLogin(@NotNull String login);

    @NotNull
    Boolean isLoginExist(@NotNull String login);

    @NotNull
    Boolean isEmailExist(@NotNull String email);

}
