package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.ISaltProvider;
import com.t1.yd.tm.api.service.IUserService;
import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.entity.UserNotFoundException;
import com.t1.yd.tm.exception.field.EmailEmptyException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.LoginEmptyException;
import com.t1.yd.tm.exception.field.PasswordEmptyException;
import com.t1.yd.tm.exception.user.IsEmailExistException;
import com.t1.yd.tm.exception.user.IsLoginExistException;
import com.t1.yd.tm.util.HashUtil;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;

public final class UserService extends AbstractService<UserDTO, IUserRepository> implements IUserService {

    @NotNull
    private final ISaltProvider saltProvider;

    public UserService(@NotNull final IConnectionService connectionService, @NotNull final ISaltProvider saltProvider) {
        super(connectionService);
        this.saltProvider = saltProvider;
    }

    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final SqlSession sqlSession) {
        return sqlSession.getMapper(IProjectRepository.class);
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final SqlSession sqlSession) {
        return sqlSession.getMapper(ITaskRepository.class);
    }

    @NotNull
    @Override
    protected IUserRepository getRepository(@NotNull final SqlSession sqlSession) {
        return sqlSession.getMapper(IUserRepository.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO removeByLogin(@NotNull final String login) {
        @Nullable final UserDTO userDTO = findByLogin(login);
        if (userDTO == null) throw new UserNotFoundException();

        removeById(userDTO.getId());
        return userDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO removeByEmail(@NotNull final String email) {
        @Nullable final UserDTO userDTO = findByEmail(email);
        if (userDTO == null) throw new UserNotFoundException();

        removeById(userDTO.getId());
        return userDTO;
    }

    @NotNull
    @SneakyThrows
    public UserDTO remove(@NotNull final UserDTO userDTOToRemove) {
        SqlSession sqlSession = connectionService.openSession();

        @Nullable final UserDTO userDTO = findOneById(userDTOToRemove.getId());
        if (userDTO == null) throw new UserNotFoundException();

        @NotNull final ITaskRepository taskRepository = getTaskRepository(sqlSession);
        taskRepository.clearWithUserId(userDTOToRemove.getId());

        @NotNull final IProjectRepository projectRepository = getProjectRepository(sqlSession);
        projectRepository.clearWithUserId(userDTOToRemove.getId());

        removeByLogin(userDTOToRemove.getLogin());

        return userDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO setPassword(@NotNull final String id, @NotNull final String password) {
        if (id.isEmpty()) throw new IdEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();

        @Nullable final UserDTO userDTO = findOneById(id);
        if (userDTO == null) throw new UserNotFoundException();
        userDTO.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        update(userDTO);
        return userDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO updateUser(@NotNull final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) {
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable final UserDTO userDTO = findOneById(id);
        if (userDTO == null) throw new UserNotFoundException();

        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setMiddleName(middleName);

        update(userDTO);
        return userDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new IsLoginExistException();

        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(login);
        userDTO.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        return add(userDTO);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new IsEmailExistException();
        if (isLoginExist(login)) throw new IsLoginExistException();

        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(login);
        userDTO.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        userDTO.setEmail(email);
        return add(userDTO);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(@NotNull final String login, @NotNull final String password, @NotNull final String email, @Nullable Role role) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) role = Role.USUAL;
        if (isLoginExist(login)) throw new IsLoginExistException();

        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(login);
        userDTO.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        userDTO.setEmail(email);
        userDTO.setRole(role);
        return add(userDTO);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO userDTO;
        try (@NotNull final SqlSession sqlSession = connectionService.openSession()) {
            @NotNull final IUserRepository repository = getRepository(sqlSession);
            userDTO = repository.findByLogin(login);
        }
        return userDTO;

    }

    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO userDTO;
        try (@NotNull final SqlSession sqlSession = connectionService.openSession()) {
            @NotNull final IUserRepository repository = getRepository(sqlSession);
            userDTO = repository.findByEmail(email);
        }
        return userDTO;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        return findByEmail(email) != null;
    }

    @Override
    @SneakyThrows
    public void lockByLogin(@NotNull final String login) {
        @Nullable final UserDTO userDTO = findByLogin(login);
        if (userDTO == null) throw new UserNotFoundException();
        userDTO.setLocked(true);
        update(userDTO);
    }

    @Override
    @SneakyThrows
    public void unlockByLogin(@NotNull final String login) {
        @Nullable final UserDTO userDTO = findByLogin(login);
        if (userDTO == null) throw new UserNotFoundException();
        userDTO.setLocked(false);
        update(userDTO);
    }

    @Override
    public @NotNull List<UserDTO> findAll(@NotNull String sort) {
        return null;
    }
}