package com.t1.yd.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.t1.yd.tm.api.service.*;
import com.t1.yd.tm.dto.model.SessionDTO;
import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.field.LoginEmptyException;
import com.t1.yd.tm.exception.field.PasswordEmptyException;
import com.t1.yd.tm.exception.user.AccessDeniedException;
import com.t1.yd.tm.exception.user.IncorrectLoginOrPasswordException;
import com.t1.yd.tm.util.CryptUtil;
import com.t1.yd.tm.util.HashUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ISaltProvider saltProvider;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    public AuthService(@NotNull final IUserService userService,
                       @NotNull final ISaltProvider saltProvider,
                       @NotNull final IPropertyService propertyService,
                       @NotNull final ISessionService sessionService) {
        this.userService = userService;
        this.saltProvider = saltProvider;
        this.propertyService = propertyService;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public UserDTO registry(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO validateToken(@Nullable String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull SessionDTO sessionDTO = objectMapper.readValue(json, SessionDTO.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = sessionDTO.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();

        if (!sessionService.existsById(sessionDTO.getId())) throw new AccessDeniedException();

        return sessionDTO;

    }

    @Override
    @NotNull
    public String login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO userDTO = userService.findByLogin(login);
        if (userDTO == null) throw new IncorrectLoginOrPasswordException();
        if (userDTO.getLocked()) throw new IncorrectLoginOrPasswordException();
        @Nullable final String hash = HashUtil.salt(password, saltProvider);
        if (hash != null && !hash.equals(userDTO.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        return getToken(userDTO);
    }

    @Override
    @SneakyThrows
    public UserDTO check(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO userDTO = userService.findByLogin(login);
        if (userDTO == null) throw new IncorrectLoginOrPasswordException();
        if (userDTO.getLocked()) throw new IncorrectLoginOrPasswordException();
        @Nullable final String hash = HashUtil.salt(password, saltProvider);
        if (hash != null && !hash.equals(userDTO.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        return userDTO;
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final UserDTO userDTO) {
        return getToken(createSession(userDTO));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDTO sessionDTO) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(sessionDTO);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private SessionDTO createSession(@NotNull final UserDTO userDTO) {
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(userDTO.getId());
        @NotNull final Role role = userDTO.getRole();
        sessionDTO.setRole(role);
        return sessionService.add(sessionDTO);
    }

    @Override
    public void logout(@Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) throw new AccessDeniedException();
        sessionService.removeById(sessionDTO.getId());
    }

}
