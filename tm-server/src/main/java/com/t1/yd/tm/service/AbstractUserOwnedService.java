package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IUserOwnedRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IUserOwnedService;
import com.t1.yd.tm.dto.model.AbstractUserOwnedEntity;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.exception.entity.EntityNotFoundException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.IndexIncorrectException;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<E extends AbstractUserOwnedEntity, R extends IUserOwnedRepository<E>> extends AbstractService<E, R> implements IUserOwnedService<E> {

    public AbstractUserOwnedService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @SneakyThrows
    public E add(@NotNull final String userId, @NotNull final E entity) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final R repository = getRepository(sqlSession);
            entity.setUserId(userId);
            repository.add(entity);
            sqlSession.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final R repository = getRepository(sqlSession);
            repository.clearWithUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final R repository = getRepository(sqlSession);
            return repository.findAllWithUserId(userId);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final R repository = getRepository(sqlSession);
            if (comparator == null) return findAll(userId);
            return repository.findAllWithUserId(userId, comparator);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    public List<E> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            @NotNull final R repository = getRepository(sqlSession);
            return repository.findOneByIdWithUserId(userId, id);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            if (index < 0) throw new IndexIncorrectException();
            @NotNull final R repository = getRepository(sqlSession);
            return repository.findOneByIndexWithUserId(userId, index);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            @NotNull final R repository = getRepository(sqlSession);
            @Nullable final E result = findOneById(userId, id);
            if (result == null) throw new EntityNotFoundException();
            repository.removeByIdWithUserId(userId, id);
            sqlSession.commit();
            return result;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            if (index < 0) throw new IndexIncorrectException();
            @NotNull final R repository = getRepository(sqlSession);
            @Nullable final E result = findOneByIndex(userId, index);
            if (result == null) throw new EntityNotFoundException();
            repository.removeByIndexWithUserId(userId, index);
            sqlSession.commit();
            return result;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            @NotNull final R repository = getRepository(sqlSession);
            return repository.findOneByIdWithUserId(userId, id) != null;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final R repository = getRepository(sqlSession);
            return repository.findAllWithUserId(userId).size();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
