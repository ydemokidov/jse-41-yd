package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IProjectTaskService;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.exception.field.ProjectIdEmptyException;
import com.t1.yd.tm.exception.field.TaskIdEmptyException;
import com.t1.yd.tm.exception.field.UserIdEmptyException;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@AllArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;


    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final SqlSession sqlSession) {
        return sqlSession.getMapper(IProjectRepository.class);
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final SqlSession sqlSession) {
        return sqlSession.getMapper(ITaskRepository.class);
    }


    @Override
    @SneakyThrows
    public void bindTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.openSession();

        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(sqlSession);
            if (projectRepository.findOneByIdWithUserId(userId, projectId) == null)
                throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = getTaskRepository(sqlSession);
            @Nullable final TaskDTO taskDTO = taskRepository.findOneByIdWithUserId(userId, taskId);
            if (taskDTO == null) throw new TaskNotFoundException();

            taskDTO.setProjectId(projectId);

            taskRepository.update(taskDTO);

            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();

        @NotNull SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(sqlSession);
            if (projectRepository.findOneByIdWithUserId(userId, projectId) == null)
                throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = getTaskRepository(sqlSession);
            @NotNull final List<TaskDTO> taskDTOS = taskRepository.findAllByProjectId(userId, projectId);
            for (@NotNull final TaskDTO taskDTO : taskDTOS) {
                taskRepository.removeById(taskDTO.getId());
            }
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(sqlSession);
            if (projectRepository.findOneByIdWithUserId(userId, projectId) == null)
                throw new ProjectNotFoundException();

            @NotNull final ITaskRepository taskRepository = getTaskRepository(sqlSession);
            @Nullable final TaskDTO taskDTO = taskRepository.findOneByIdWithUserId(userId, taskId);
            if (taskDTO == null) throw new TaskNotFoundException();

            taskDTO.setProjectId(null);

            taskRepository.update(taskDTO);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
