package com.t1.yd.tm.api.service;

import com.t1.yd.tm.dto.model.AbstractUserOwnedEntity;
import com.t1.yd.tm.enumerated.Sort;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<E extends AbstractUserOwnedEntity> extends IService<E> {

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Sort sort);

    void clear(@NotNull String userId);

    E add(@NotNull String userId, @NotNull E entity);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @Nullable Comparator comparator);

    @Nullable
    E findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    E findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    E removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeByIndex(@NotNull String userId, @NotNull Integer index);

    boolean existsById(@NotNull String userId, @NotNull String id);

    int getSize(@NotNull String userId);

}
