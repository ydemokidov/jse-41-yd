package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.ISessionRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.ISessionService;
import com.t1.yd.tm.dto.model.SessionDTO;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class SessionService extends AbstractUserOwnedService<SessionDTO, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository(@NotNull final SqlSession sqlSession) {
        return sqlSession.getMapper(ISessionRepository.class);
    }


    @Override
    public @NotNull SessionDTO update(@NotNull SessionDTO entity) {
        throw new NotImplementedException();
    }

}
