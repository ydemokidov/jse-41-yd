package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.dto.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(@NotNull E entity);

    void clear();

    @NotNull
    List<E> findAll();

    @NotNull
    Collection<E> set(@NotNull Collection<E> collection);

    @NotNull
    Collection<E> add(@NotNull Collection<E> collection);

    @NotNull
    List<E> findAll(@NotNull Comparator comparator);

    @Nullable
    E findOneById(@NotNull String id);

    @Nullable
    E findOneByIndex(@NotNull Integer index);

    void removeById(@NotNull String id);

    void removeByIndex(@NotNull Integer index);

    boolean existsById(@NotNull String id);

    int getSize();

    void update(@NotNull E entity);

}
