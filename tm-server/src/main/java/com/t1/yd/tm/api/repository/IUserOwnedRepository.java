package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.dto.model.AbstractUserOwnedEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<E extends AbstractUserOwnedEntity> extends IRepository<E> {

    void clearWithUserId(@NotNull String userId);

    @NotNull
    List<E> findAllWithUserId(@NotNull String userId);

    @NotNull
    List<E> findAllWithUserId(@NotNull String userId, @NotNull Comparator comparator);

    @Nullable
    E findOneByIdWithUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    E findOneByIndexWithUserId(@NotNull String userId, @NotNull Integer index);

    void removeByIdWithUserId(@NotNull String userId, @NotNull String id);

    void removeByIndexWithUserId(@NotNull String userId, @NotNull Integer index);

    boolean existsById(@NotNull String userId, @NotNull String id);

    int getSizeWithUserId(@NotNull String userId);

}