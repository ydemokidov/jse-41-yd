package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.*;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.t1.yd.tm.constant.ProjectTestData.USER_1_PROJECT_DTO_1;
import static com.t1.yd.tm.constant.TaskTestData.USER_1_PROJECT_1_TASK_DTO_1;
import static com.t1.yd.tm.constant.UserTestData.USER_DTO_1;

@Category(UnitCategory.class)
public class ProjectDTOTaskDTOServiceTest {

    private IPropertyService propertyService = new PropertyService();

    private IConnectionService connectionService = new ConnectionService(propertyService);

    private IProjectService projectService;

    private ITaskService taskService;

    private IProjectTaskService service;

    @Before
    public void initRepository() {
        service = new ProjectTaskService(connectionService);
        projectService = new ProjectService(connectionService);
        taskService = new TaskService(connectionService);
    }

    @Test
    public void bindTaskToProject() {
        projectService.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        taskService.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.bindTaskToProject(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        @NotNull final TaskDTO bindedTaskDTO = taskService.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId());
        Assert.assertEquals(USER_1_PROJECT_DTO_1.getId(), bindedTaskDTO.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        projectService.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        taskService.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.bindTaskToProject(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        service.unbindTaskFromProject(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        Assert.assertTrue(taskService.findAllByProjectId(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).isEmpty());
    }

    @Test
    public void removeProjectById() {
        projectService.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        taskService.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.bindTaskToProject(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        service.removeProjectById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        Assert.assertTrue(taskService.findAllByProjectId(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).isEmpty());
    }

    @After
    public void clearData() {
        taskService.clear();
        projectService.clear();
    }

}