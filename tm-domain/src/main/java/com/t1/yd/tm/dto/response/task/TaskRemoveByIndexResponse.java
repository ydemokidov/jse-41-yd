package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.dto.model.TaskDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class TaskRemoveByIndexResponse extends AbstractTaskResponse {

    public TaskRemoveByIndexResponse(TaskDTO taskDTO) {
        super(taskDTO);
    }

    public TaskRemoveByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
