package com.t1.yd.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractUserOwnedEntity extends AbstractEntity {

    @Nullable
    @Column(name = "user_id", length = 50)
    protected String userId;

    public AbstractUserOwnedEntity(@Nullable String userId) {
        this.userId = userId;
    }

}
