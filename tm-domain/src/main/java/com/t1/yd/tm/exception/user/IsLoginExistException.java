package com.t1.yd.tm.exception.user;

public class IsLoginExistException extends AbstractUserException {

    public IsLoginExistException() {
        super("Error! Login exists...");
    }

}
