package com.t1.yd.tm.exception.entity;

public class EntityNotFoundException extends AbstractEntityNotFoundException {

    public EntityNotFoundException() {
        super("Error! Entity not found...");
    }

}
